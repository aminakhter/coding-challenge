// Angular Js
var app = angular.module('insurance', []);
app.controller('MainCtrl', function($scope,$http) {
  $http.get('data/sampleData.json').success(function(data) {
	   $scope.order = '';
	   $scope.filter = 'autoGapInsurance';
       $scope.jsonFileData = data;
    });    
});

// jQuery Custom Functions
$(document).ready(function(){
  //event plan
  $(".plan").on("click", function(){
    app.selectPlan(this);
  });
  //event compare
  $(".comp").on("click", function(){
    app.comp();
  });
  //event reset
  $(".reset").on("click", function(){
    app.reset();
  });
  
});

app = {
  //Select/Deselect Plan
  selectPlan:function(elem){
    $(elem).toggleClass('selected');
    if($(".plan.selected").length < 2){
      $(".comp").fadeOut();
    }else{
      $(".comp").fadeIn();
    }
  },
  //Compare Button
  comp:function(){
    var elem = $(".plan");
    if(elem.hasClass('selected')){
     $(".plan:not(.selected)").hide();
    }
  },
  //Reset Button
  reset:function(){
    var elem = $(".plan");
    elem.removeClass('selected');
    elem.slideDown(2000);
    $(".comp").fadeOut(1000);  
  }
}